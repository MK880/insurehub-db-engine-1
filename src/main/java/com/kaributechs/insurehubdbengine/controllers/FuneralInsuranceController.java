package com.kaributechs.insurehubdbengine.controllers;

import com.kaributechs.insurehubdbengine.models.new_business.funeral_insurance.FuneralInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.funeral_insurance.FuneralInsuranceDTO;
import com.kaributechs.insurehubdbengine.models.new_business.life_insurance.LifeInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.life_insurance.LifeInsuranceDTO;
import com.kaributechs.insurehubdbengine.services.IFuneralInsuranceService;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/funeral-insurance")
@Slf4j
@Hidden
public class FuneralInsuranceController {
    private final IFuneralInsuranceService funeralInsuranceService;

    public FuneralInsuranceController(IFuneralInsuranceService funeralInsuranceService) {
        this.funeralInsuranceService = funeralInsuranceService;
    }

    @PostMapping("/save")
    public FuneralInsurance apply(@RequestBody @Valid FuneralInsuranceDTO funeralInsuranceDTO){
        return funeralInsuranceService.save(funeralInsuranceDTO);
    }

    @GetMapping("/get/{processId}")
    public FuneralInsurance getInsuranceByProcessId(@PathVariable String processId){
        log.info("Get Personal Vehicle Insurance by process id {}",processId);
        return funeralInsuranceService.getInsuranceByProcessId(processId);
    }
}
