package com.kaributechs.insurehubdbengine.controllers;

import com.kaributechs.insurehubdbengine.models.dealer.DealerRequest;
import com.kaributechs.insurehubdbengine.models.dealer.DealerRequestQuotationDTO;
import com.kaributechs.insurehubdbengine.models.dealer.DealerService;
import com.kaributechs.insurehubdbengine.models.dealer.Service;
import com.kaributechs.insurehubdbengine.services.IDealerRequestService;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/dealer-request")
@Hidden
@Slf4j
public class DealerRequestController {
    private final IDealerRequestService dealerService;

    public DealerRequestController(IDealerRequestService dealerService) {
        this.dealerService = dealerService;
    }

    @PostMapping("/add")
    public DealerRequest addDealerRequest(@Valid @RequestBody DealerRequestQuotationDTO dealerRequestQuotationDTO){
        log.info("Saving dealer request : {}", dealerRequestQuotationDTO);
        return dealerService.addDealerRequestImp(dealerRequestQuotationDTO);
    }

    @GetMapping("/{id}")
    public DealerRequest getDealerRequestById(@PathVariable Long id){
        return dealerService.getDealerRequestById(id);
    }

    @GetMapping("/process/{processId}")
    public DealerRequest getDealerRequestByProcessInstanceId(@PathVariable String processId){
        return dealerService.getDealerRequestByProcessInstanceId(processId);
    }

    @GetMapping("/all/{username}")
    public List<DealerRequest> getDealerRequestByUsername(@PathVariable String username){
        return dealerService.getDealerRequestByUsername(username);
    }

    @GetMapping("/service")
    public List<DealerService> getDealerRequestsByService(@RequestBody Service service){
        return dealerService.getDealerRequestsByService(service);
    }



}
