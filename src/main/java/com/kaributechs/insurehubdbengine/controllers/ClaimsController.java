package com.kaributechs.insurehubdbengine.controllers;


import com.kaributechs.insurehubdbengine.models.claims.Claim;
import com.kaributechs.insurehubdbengine.models.dtos.ClaimDTO;
import com.kaributechs.insurehubdbengine.models.dtos.ResponseDTO;
import com.kaributechs.insurehubdbengine.services.IClaimService;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
@Hidden
@Slf4j
public class ClaimsController {
    private final IClaimService claimService;

    public ClaimsController(IClaimService claimService) {

        this.claimService = claimService;
    }

    @PostMapping("/claim/add")
    public Claim addClaim(@Valid @RequestBody ClaimDTO claimDTO){
        log.info("Claim : {}",claimDTO);
        return claimService.addClaimImp(claimDTO);
    }

    @GetMapping("/claim/{id}")
    public Claim getClaimById(@PathVariable String id){
        return claimService.getClaimByIdImp(id);
    }

    @GetMapping("/claim/processId/{processId}")
    public Claim getClaimByProcessInstanceId(@PathVariable String processId){
        return claimService.getClaimByProcessInstanceId(processId);
    }

    @GetMapping("/claims/{username}")
    public List<Claim> getClaimByUsername(@PathVariable String username){
        return claimService.getClaimByUsername(username);
    }

    @GetMapping("/claims")
    public List<Claim> getAllClaims(){
        return claimService.getAllClaimsImp();
    }

    @PostMapping("/claim/{id}/delete")
    public ResponseDTO deleteClaim(@PathVariable String id){
        return claimService.deleteClaimImp(id);
    }

    @PatchMapping("/claim/update")
    public Claim updateClaim(@RequestBody Claim claim){
        return  claimService.updateClaimImp(claim);
    }
}
