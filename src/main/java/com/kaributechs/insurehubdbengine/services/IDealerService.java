package com.kaributechs.insurehubdbengine.services;

import com.kaributechs.insurehubdbengine.models.dealer.RegisterDealerDTO;
import com.kaributechs.insurehubdbengine.models.dealer.Dealer;
import com.kaributechs.insurehubdbengine.models.dealer.AddDealerServiceDTO;

public interface IDealerService {
    Dealer registerDealer(RegisterDealerDTO username);

    Dealer addDealerService(AddDealerServiceDTO service);

    Dealer getDealerByUsername(String username);
}
