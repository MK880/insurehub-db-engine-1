package com.kaributechs.insurehubdbengine.services.implementation;

import com.kaributechs.insurehubdbengine.exceptions.GenericException;
import com.kaributechs.insurehubdbengine.models.Template;
import com.kaributechs.insurehubdbengine.models.dtos.EditTemplateDTO;
import com.kaributechs.insurehubdbengine.repositories.TemplateRepository;
import com.kaributechs.insurehubdbengine.services.ITemplatesService;
import com.kaributechs.insurehubdbengine.utilities.Utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;


@Service
public class TemplatesServiceImp implements ITemplatesService {
    private final TemplateRepository templateRepository;
    private final Utilities utils;
    private final StaticAttachmentServiceImp staticAttachmentServiceImp;
    Logger logger= LoggerFactory.getLogger(this.getClass());


    public TemplatesServiceImp(TemplateRepository templateRepository, Utilities utils, StaticAttachmentServiceImp staticAttachmentServiceImp) {
        this.templateRepository = templateRepository;
        this.utils = utils;
        this.staticAttachmentServiceImp = staticAttachmentServiceImp;
    }

    @Override
    public Template getTemplateByIdImp(Long id) {
        return templateRepository.findById(id).orElseThrow(() -> new GenericException(String.format("Template with id %s not found", id)));
    }

    @Override
    public Template editTemplateImp(EditTemplateDTO editTemplateDTO) {
        Template template = getTemplateByIdImp(editTemplateDTO.getTemplateId());
        template.setFooterImageUrl(editTemplateDTO.getFooterImageUrl());
        template.setHeaderImageUrl(editTemplateDTO.getHeaderImageUrl());
        template.setSignature(editTemplateDTO.getSignature());
        return templateRepository.save(template);
    }



}
