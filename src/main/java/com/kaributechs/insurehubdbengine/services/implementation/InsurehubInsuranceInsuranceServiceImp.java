package com.kaributechs.insurehubdbengine.services.implementation;

import com.kaributechs.insurehubdbengine.exceptions.ResourceNotFound;
import com.kaributechs.insurehubdbengine.models.InsurehubInsurance;
import com.kaributechs.insurehubdbengine.models.InsurehubInsuranceDTO;
import com.kaributechs.insurehubdbengine.repositories.InsurehubInsuranceRepository;
import com.kaributechs.insurehubdbengine.services.IInsurehubInsuranceService;
import com.kaributechs.insurehubdbengine.utilities.Utilities;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InsurehubInsuranceInsuranceServiceImp implements IInsurehubInsuranceService {
    private final InsurehubInsuranceRepository insurehubInsuranceRepository;

    public InsurehubInsuranceInsuranceServiceImp(InsurehubInsuranceRepository insurehubInsuranceRepository) {
        this.insurehubInsuranceRepository = insurehubInsuranceRepository;
    }

    @Override
    public InsurehubInsurance addInsurance(InsurehubInsuranceDTO insurehubInsuranceDTO) {
        InsurehubInsurance insurehubInsurance=new InsurehubInsurance();
        BeanUtils.copyProperties(insurehubInsuranceDTO,insurehubInsurance);
        return insurehubInsuranceRepository.save(insurehubInsurance);
    }

    @Override
    public InsurehubInsurance getInsuranceById(Long id) {
        return insurehubInsuranceRepository.findById(id).orElseThrow(()->new ResourceNotFound("Insurehub Insurance","id",id));
    }

    @Override
    public InsurehubInsurance updateInsuranceData(InsurehubInsurance insurehubInsurance) {
        InsurehubInsurance existingInsurance=getInsuranceById(insurehubInsurance.getId());
        BeanUtils.copyProperties(insurehubInsurance,existingInsurance, Utilities.getNullPropertyNames(insurehubInsurance));
        return insurehubInsuranceRepository.save(existingInsurance);
    }

    @Override
    public List<InsurehubInsurance> getAllInsurances() {
        return insurehubInsuranceRepository.findAll();
    }
}
