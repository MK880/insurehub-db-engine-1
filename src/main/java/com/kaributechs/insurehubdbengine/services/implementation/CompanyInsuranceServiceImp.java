package com.kaributechs.insurehubdbengine.services.implementation;

import com.kaributechs.insurehubdbengine.exceptions.AlreadyExists;
import com.kaributechs.insurehubdbengine.exceptions.ResourceNotFound;
import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.InsurehubInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.CompanyInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.UpdateCompanyInsuranceDTO;
import com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance.AddCompanyInsuranceDTO;
import com.kaributechs.insurehubdbengine.repositories.CompanyInsuranceRepository;
import com.kaributechs.insurehubdbengine.repositories.CompanyRepository;
import com.kaributechs.insurehubdbengine.services.ICompanyInsuranceService;
import com.kaributechs.insurehubdbengine.services.ICompanyService;
import com.kaributechs.insurehubdbengine.services.IInsurehubInsuranceService;
import com.kaributechs.insurehubdbengine.utilities.Utilities;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class CompanyInsuranceServiceImp implements ICompanyInsuranceService {
    private final IInsurehubInsuranceService insurehubInsuranceService;
    private final ICompanyService companyService;
    private final CompanyRepository companyRepository;
    private final CompanyInsuranceRepository companyInsuranceRepository;

    public CompanyInsuranceServiceImp(IInsurehubInsuranceService insurehubInsuranceService, ICompanyService companyService, CompanyRepository companyRepository, CompanyInsuranceRepository companyInsuranceRepository) {
        this.insurehubInsuranceService = insurehubInsuranceService;
        this.companyService = companyService;
        this.companyRepository = companyRepository;
        this.companyInsuranceRepository = companyInsuranceRepository;
    }

    @Override
    public Company addCompanyInsurance(AddCompanyInsuranceDTO addCompanyInsuranceDTO) {
        CompanyInsurance companyInsurance=new CompanyInsurance();
        Company company=companyService.getCompanyByIdImp(addCompanyInsuranceDTO.getCompanyId());
        companyInsurance.setCompany(company);

        BeanUtils.copyProperties(addCompanyInsuranceDTO,companyInsurance);

        InsurehubInsurance insurehubInsurance=insurehubInsuranceService.getInsuranceById(addCompanyInsuranceDTO.getInsuranceId());
        BeanUtils.copyProperties(insurehubInsurance,companyInsurance);
        if (insurehubInsurance.getKey().equals("vehicleinsurance")||insurehubInsurance.getKey().equals("companyvehicleinsurance")){
            companyInsurance.setChildBeneficiaryPremium(null);
            companyInsurance.setAdultBeneficiaryPremium(null);
            companyInsurance.setSeniorBeneficiaryPremium(null);
        }
        companyInsurance.setInsurehubInsurance(insurehubInsurance);

        if (company.getInsurances()==null){
            Set<CompanyInsurance> insurances=new HashSet<>();
            insurances.add(companyInsurance);
            company.setInsurances(insurances);
        }else{
            company.getInsurances().forEach(insurance->{
                if (insurance.getKey().equals(insurehubInsurance.getKey())){
                    throw new AlreadyExists("Company Insurance","key",insurance.getKey());
                }
            });
            company.getInsurances().add(companyInsurance);
        }

        return companyRepository.save(company);
    }

    @Override
    public CompanyInsurance getCompanyInsuranceById(Long id) {
        return companyInsuranceRepository.findById(id).orElseThrow(()->new ResourceNotFound("Company Insurance","id",id));
    }

    @Override
    public CompanyInsurance updateCompanyInsurance(UpdateCompanyInsuranceDTO updateCompanyInsuranceDTO) {
        CompanyInsurance existingCompanyInsurance=getCompanyInsuranceById(updateCompanyInsuranceDTO.getInsuranceId());
        BeanUtils.copyProperties(updateCompanyInsuranceDTO,existingCompanyInsurance, Utilities.getNullPropertyNames(updateCompanyInsuranceDTO));
        return companyInsuranceRepository.save(existingCompanyInsurance);
    }
}

