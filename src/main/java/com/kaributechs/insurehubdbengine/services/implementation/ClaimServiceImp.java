package com.kaributechs.insurehubdbengine.services.implementation;


import com.kaributechs.insurehubdbengine.exceptions.ResourceNotFound;
import com.kaributechs.insurehubdbengine.models.claims.*;
import com.kaributechs.insurehubdbengine.models.dtos.ClaimDTO;
import com.kaributechs.insurehubdbengine.models.dtos.ResponseDTO;
import com.kaributechs.insurehubdbengine.repositories.ClaimRepository;
import com.kaributechs.insurehubdbengine.services.IClaimService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class ClaimServiceImp implements IClaimService {


    private final ClaimRepository claimRepository;

    public ClaimServiceImp(ClaimRepository claimRepository) {
        this.claimRepository = claimRepository;
    }

    @Override
    public Claim addClaimImp(ClaimDTO claimDTO) {
        Date date=new Date();
        Claim claim=mapClaimData(claimDTO);
        claim.setClaimCreationDate(date);
        return claimRepository.save(claim);
    }

    private Claim mapClaimData(ClaimDTO claimDTO){
        Claim claim=new Claim();
        BeanUtils.copyProperties(claimDTO,claim);
        BeanUtils.copyProperties(claimDTO.getClaimsWorkflowRequestDTO(),claim);

        ClaimProcessDetails processDetails=new ClaimProcessDetails();
        claim.setProcessDetails(processDetails);

        ClaimClientDetails clientDetails= new ClaimClientDetails();
        BeanUtils.copyProperties(claimDTO.getClaimsWorkflowRequestDTO().getClientDetails(),clientDetails);
        claim.setClientDetails(clientDetails);

        ClaimInsuredDetails insuredDetails=new ClaimInsuredDetails();
        BeanUtils.copyProperties(claimDTO.getClaimsWorkflowRequestDTO().getInsuredDetails(),insuredDetails);
        claim.setInsuredDetails(insuredDetails);

        ClaimVehicleDetails vehicleDetails=new ClaimVehicleDetails();
        BeanUtils.copyProperties(claimDTO.getClaimsWorkflowRequestDTO().getVehicleDetails(),vehicleDetails);
        claim.setVehicleDetails(vehicleDetails);

        ClaimDriverDetails driverDetails=new ClaimDriverDetails();
        BeanUtils.copyProperties(claimDTO.getClaimsWorkflowRequestDTO().getDriverDetails(),driverDetails);
        claim.setDriverDetails(driverDetails);

        ClaimAccidentDetails accidentDetails=new ClaimAccidentDetails();
        BeanUtils.copyProperties(claimDTO.getClaimsWorkflowRequestDTO().getAccidentDetails(),accidentDetails);
        claim.setAccidentDetails(accidentDetails);

        log.info("Claim : {}",claim);

        return claim;
    }


    @Override
    public Claim getClaimByIdImp(String id) {
        return claimRepository.findByIdAndDeleted(id,false).orElseThrow(()-> new ResourceNotFound("Claim","id",id));
    }

    @Override
    public Claim getClaimByProcessInstanceId(String processId) {
        return claimRepository.findByProcessInstanceIdAndDeleted(processId,false).orElseThrow(()-> new ResourceNotFound("Claim","process Id",processId));
    }

    @Override
    public List<Claim> getAllClaimsImp() {
        return claimRepository.findAllByDeleted(false);
    }

    @Override
    public ResponseDTO deleteClaimImp(String id) {
        Claim claim=getClaimByIdImp(id);
        claim.setDeleted(true);
        claimRepository.save(claim);
        return new ResponseDTO(String.format("Claim with id %s deleted successfully",id));
    }

    @Override
    public Claim updateClaimImp(Claim claim) {
        Claim existingClaim=getClaimByIdImp(claim.getId());
        existingClaim.setUsername(claim.getUsername());
        existingClaim.setStatus(claim.getStatus());
        return claimRepository.save(existingClaim);
    }

    @Override
    public List<Claim> getClaimByUsername(String username) {
        return claimRepository.findAllByUsernameAndDeleted(username,false);
    }
}
