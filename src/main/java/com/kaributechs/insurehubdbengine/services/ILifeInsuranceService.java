package com.kaributechs.insurehubdbengine.services;

import com.kaributechs.insurehubdbengine.models.new_business.life_insurance.LifeInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.life_insurance.LifeInsuranceDTO;

public interface ILifeInsuranceService {

    LifeInsurance save(LifeInsuranceDTO lifeInsuranceDTO);

    LifeInsurance getInsuranceByProcessId(String processId);
}
