package com.kaributechs.insurehubdbengine.services;

import com.kaributechs.insurehubdbengine.models.EmailAttachment;
import org.springframework.web.multipart.MultipartFile;

public interface IEmailAttachmentService {
    EmailAttachment saveEmailAttachment(MultipartFile emailAttachment);

    EmailAttachment getEmailAttachmentById(Long id);
}
