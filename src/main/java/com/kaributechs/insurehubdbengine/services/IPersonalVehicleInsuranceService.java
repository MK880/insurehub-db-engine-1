package com.kaributechs.insurehubdbengine.services;

import com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance.PersonalVehicleInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance.PersonalVehicleInsuranceDTO;

public interface IPersonalVehicleInsuranceService {
    PersonalVehicleInsurance apply(PersonalVehicleInsuranceDTO personalVehicleInsuranceDTO);

    PersonalVehicleInsurance getInsuranceByProcessId(String processId);
}
