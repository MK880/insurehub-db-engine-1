package com.kaributechs.insurehubdbengine.services;

import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.Template;
import com.kaributechs.insurehubdbengine.models.dtos.CreateTemplateDTO;
import com.kaributechs.insurehubdbengine.models.dtos.GetCompanyTemplateIdDTO;
import com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance.AddCompanyInsuranceDTO;

import java.util.List;

public interface ICompanyService {
    Company addCompanyImp(Company company);

    List<Company> getAllCompaniesImp();

    Company createTemplate(CreateTemplateDTO createTemplateDTO);

    List<Template> getCompanyTemplatesImp(Long id);

    Company getCompanyByIdImp(Long id);

    Company updateCompanyImp(Company company);

    Long getCompanyTemplateIdImp(GetCompanyTemplateIdDTO getCompanyTemplateIdDTO);

}
