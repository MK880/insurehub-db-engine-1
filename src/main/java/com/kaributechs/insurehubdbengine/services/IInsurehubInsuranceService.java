package com.kaributechs.insurehubdbengine.services;

import com.kaributechs.insurehubdbengine.models.InsurehubInsurance;
import com.kaributechs.insurehubdbengine.models.InsurehubInsuranceDTO;

import java.util.List;

public interface IInsurehubInsuranceService {
    InsurehubInsurance addInsurance(InsurehubInsuranceDTO insurehubInsuranceDTO);

    InsurehubInsurance getInsuranceById(Long id);

    InsurehubInsurance updateInsuranceData(InsurehubInsurance insurehubInsurance);

    List<InsurehubInsurance> getAllInsurances();

}
