package com.kaributechs.insurehubdbengine.models.claims.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ClaimClientDetailsDTO {
    private Long clientId;
    private String clientFirstName;
    private String clientLastName;

    @NotNull(message="Personal details cannot be null")
    private String clientEmail;
    private String clientPhoneNumber;
    private String clientIdNumber;
    private String clientType;
    private String businessName;
    private String businessRegistrationNumber;
}
