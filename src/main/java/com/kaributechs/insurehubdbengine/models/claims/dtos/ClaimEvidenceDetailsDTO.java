package com.kaributechs.insurehubdbengine.models.claims.dtos;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class ClaimEvidenceDetailsDTO {
    private MultipartFile[] policeReport;
    private MultipartFile[] videoEvidence;
    private MultipartFile[] audioEvidence;
    private MultipartFile[] additionalDocuments;
}
