package com.kaributechs.insurehubdbengine.models.dtos;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class TemplateEmailDTO {
    private Long templateId;
    private String recipient;
    private String fullName;
    private String subject;
    private String recipientAddress;
    private String staticAttachments;
    private MultipartFile[] attachments;
}
