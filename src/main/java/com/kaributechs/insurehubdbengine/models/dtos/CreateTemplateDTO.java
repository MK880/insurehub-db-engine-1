package com.kaributechs.insurehubdbengine.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CreateTemplateDTO {
    private Long companyId;
    private Long templateBlueprintId;
}
