package com.kaributechs.insurehubdbengine.models.dtos;

import com.kaributechs.insurehubdbengine.models.claims.dtos.ClaimsWorkflowRequestDTO;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@ToString
@AllArgsConstructor
public class ClaimDTO {


    private String id;

    private String username;

    private String processInstanceId;

    private String insuranceType;

    private Date claimCreationDate;

    private Date lastClaimStatusChangeDate;

    private String status;

    private Long companyId;


    private boolean deleted=false;
     private ClaimsWorkflowRequestDTO claimsWorkflowRequestDTO;
}
