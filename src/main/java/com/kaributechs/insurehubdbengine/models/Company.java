package com.kaributechs.insurehubdbengine.models;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.kaributechs.insurehubdbengine.models.new_business.CompanyInsurance;
import com.kaributechs.insurehubdbengine.models.insurance_quotation.NewBusinessCompanyQuote;
import com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance.CompanyVehicleInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.funeral_insurance.FuneralInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.household_insurance.HouseholdInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.life_insurance.LifeInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.medical_insurance.MedicalInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance.PersonalVehicleInsurance;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String email;
    private String phoneNumber;
    @OneToMany(mappedBy = "company",
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER )
    @JsonManagedReference("company_template")
    private List<Template> templates;

    @OneToMany(mappedBy = "company",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
            )
    @JsonManagedReference("company_static_attachment")
    private Set<StaticAttachment> staticAttachments;


    @OneToMany(mappedBy = "company",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("company_insurances")
    private Set<CompanyInsurance> insurances;

    @OneToMany(mappedBy = "company",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("company_personalVehicleInsuranceClients")
    private Set<PersonalVehicleInsurance> personalVehicleInsuranceClients;

    @OneToMany(mappedBy = "company",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("company_medicalInsuranceClients")
    private Set<MedicalInsurance> medicalInsuranceClients;

    @OneToMany(mappedBy = "company",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("company_householdInsuranceClients")
    private Set<HouseholdInsurance> householdInsuranceClients;

    @OneToMany(mappedBy = "company",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("company_funeralInsuranceClients")
    private Set<FuneralInsurance> funeralInsuranceClients;

    @OneToMany(mappedBy = "company",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("company_lifeInsuranceClients")
    private Set<LifeInsurance> lifeInsuranceClients;

    @OneToMany(mappedBy = "company",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("company_companyVehicleInsuranceClients")
    private Set<CompanyVehicleInsurance> companyVehicleInsuranceClients;

    @OneToMany(mappedBy = "company",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("company_quotations")
    private Set<NewBusinessCompanyQuote> quotations;


}
