package com.kaributechs.insurehubdbengine.models.new_business.medical_insurance;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Data
public class MedicalInsuranceMemberDetailsDTO {

    private String lastName;
    private String firstName;
    private String salutation;
    private String nationalId;
    private Date dateOfBirth;
    private String gender;
    private String maritalStatus;
    private String identificationType;
}
