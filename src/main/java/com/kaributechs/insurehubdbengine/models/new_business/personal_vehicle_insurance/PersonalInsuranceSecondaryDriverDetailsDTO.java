package com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
public class PersonalInsuranceSecondaryDriverDetailsDTO {
    private boolean isVehicleUsedByOthers;
    private String dependentLicenseNumber;
    private String dependentDateWhenLicenseWasObtained;
    private String dependentNationalId;
    private String relationshipToBeneficiary;
    private String dependentFirstName;
    private String dependentLastName;
    private String dependentEmailAddress;
    private String dependentDateOfBirth;
    private String dependentPhoneNumber;
    private String dependentGender;
    private String SecDriverIdentificationType;
}
