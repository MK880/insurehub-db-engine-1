package com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class PersonalDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String salutation;
    private String nationalId;
    private String clientFirstName;
    private String clientLastName;
    private String dateOfBirth;
    private String clientGender;
    private String maritalStatus;
    private String identificationType;
}
