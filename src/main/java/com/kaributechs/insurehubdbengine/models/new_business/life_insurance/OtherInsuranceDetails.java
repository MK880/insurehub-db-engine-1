package com.kaributechs.insurehubdbengine.models.new_business.life_insurance;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class OtherInsuranceDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String amountOfInsurance;
    private String yearOfIssue;
    private String status;
}
