package com.kaributechs.insurehubdbengine.models.new_business.household_insurance;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
public class HouseholdInsurancePropertyAddressDTO {

    private boolean isPropertyAddressResidentialAddress;
    private String streetAddress;
    private String suburb;
    private String city;
    private String country;
    private String region;
    private String zipCode;
}
