package com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance;

import com.kaributechs.insurehubdbengine.models.new_business.ContactDetailsDTO;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
public class CompanyVehicleInsuranceDTO {

    @NotNull(message="companyId cannot be null")
    private Long companyId;

    @Hidden
    private String processId;

    @NotNull(message="Company details cannot be null")
    private CompanyDetailsDTO companyDetails;

    @NotNull(message="Contact details cannot be null")
    private ContactDetailsDTO contactDetails;

    @Valid
    private Set<CompanyInsuranceVehicleDetailsDTO> vehicles;

    @NotNull(message="Insurance questions cannot be null")
    private CompanyInsuranceQuestionsDTO insuranceQuestions;




}
