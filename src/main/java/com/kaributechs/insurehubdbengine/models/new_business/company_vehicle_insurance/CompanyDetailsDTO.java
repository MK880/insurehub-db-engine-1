package com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
public class CompanyDetailsDTO {
    private String companyClass;
    private String companyName;
    private String cin;
    private String registrationNumber;
    private String businessDescription;
}
