package com.kaributechs.insurehubdbengine.models.new_business;

import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.InsurehubInsurance;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class CompanyInsuranceDTO {

    private Long id;
    private String name;
    private String key;

    private BigDecimal principalMemberPremium;
    private BigDecimal childBeneficiaryPremium;
    private BigDecimal adultBeneficiaryPremium;
    private BigDecimal seniorBeneficiaryPremium;

    private Company company;

    private InsurehubInsurance insurehubInsurance;

}
