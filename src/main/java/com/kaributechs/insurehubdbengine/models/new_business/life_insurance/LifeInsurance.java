package com.kaributechs.insurehubdbengine.models.new_business.life_insurance;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.new_business.ContactDetails;
import com.kaributechs.insurehubdbengine.models.new_business.SpouseDetails;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class LifeInsurance {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String processId;


    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private LifeInsuranceMemberDetails memberDetails;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private ContactDetails contactDetails;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private SpouseDetails spouse;

    @OneToMany(mappedBy = "lifeInsurance",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("lifeInsurance_beneficiaries")
    private Set<LifeInsuranceBeneficiaryDetails> beneficiaries;

    @OneToMany(mappedBy = "lifeInsurance",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("lifeInsurance_extendedFamily")
    private Set<LifeInsuranceExtendedFamilyDetails> extendedFamily;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private LifeInsuranceQuestions insuranceQuestions;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private OtherInsuranceDetails otherInsuranceDetails;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("company_lifeInsuranceClients")
    private Company company;


}
