package com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance;

import com.kaributechs.insurehubdbengine.models.new_business.VehicleUserDetailsDTO;
import com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance.OtherVehicleUserDetailsDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class CompanyInsuranceVehicleDetailsDTO {
    private String registrationNumber;
    private String make;
    private String year;
    private String type;
    private String mileage;
    private String worth;
    private String usage;
    private String numberOfSeats;
    private String alarmSystemYesOrNo;
    private String nightParkingYesOrNo;
    private boolean vehicleUsedByOthers;
    private String takenHome;

    @NotNull(message="Vehicle user cannot be null")
    private VehicleUserDetailsDTO vehicleUser;

    private List<OtherVehicleUserDetailsDTO> otherVehicleUsers;

}
