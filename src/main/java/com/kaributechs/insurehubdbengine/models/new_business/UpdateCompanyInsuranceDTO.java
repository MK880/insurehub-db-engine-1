package com.kaributechs.insurehubdbengine.models.new_business;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class UpdateCompanyInsuranceDTO {

    @NotNull(message = "insuranceId cannot be null")
    private Long insuranceId;

    private BigDecimal principalMemberPremium;
    private BigDecimal childBeneficiaryPremium;
    private BigDecimal adultBeneficiaryPremium;
    private BigDecimal seniorBeneficiaryPremium;
}
