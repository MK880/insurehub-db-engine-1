package com.kaributechs.insurehubdbengine.models.new_business.medical_insurance;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
@Entity
@Getter
@Setter
@NoArgsConstructor
public class MedicalInsuranceExtendedFamilyDetails  {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String lastName;
    private String firstName;
    private String salutation;
    private String nationalId;
    private Date dateOfBirth;
    private String emailAddress;
    private String phoneNumber;
    private String gender;
    private String maritalStatus;
    private String identificationType;
    private String relationshipToMember;
    private String funeralCover;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("medicalInsurance_extendedFamily")
    private MedicalInsurance medicalInsurance;
}
