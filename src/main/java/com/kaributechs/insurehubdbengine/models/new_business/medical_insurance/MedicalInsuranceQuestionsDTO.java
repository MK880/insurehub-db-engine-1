package com.kaributechs.insurehubdbengine.models.new_business.medical_insurance;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
public class MedicalInsuranceQuestionsDTO {
    private String insuranceType;
    private String insurancePremiumRange;
    private String paymentFrequency;
    private String insuranceCurrency;
    private String condolenceFee;
    private String excessYesOrNo;
    private String extendedFamilyBenefits;
    private String periodOfInsuranceFrom;
    private String maritalStatus;
}
