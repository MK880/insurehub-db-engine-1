package com.kaributechs.insurehubdbengine.models.new_business.funeral_insurance;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class FuneralInsuranceQuestions {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String benefits_coffin;
    private String benefits_grocery;
    private String benefits_bus;
    private String benefits_repatriation;
    private String benefits_condolenceFee;
    private String insuranceQuestions_excessYesOrNo;
    private String benefits_extendedFamilyBenefits;
    private String insuranceQuestions_periodOfInsuranceFrom;
    private String maritalStatus;
}
