package com.kaributechs.insurehubdbengine.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@NoArgsConstructor
@Getter
@Setter@ToString
public class EmailAttachment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String filename;

    private String type;

    @CreationTimestamp
    private Date dateAdded;

    @Lob
    private byte[] data;

    public EmailAttachment(Long id, String filename, String type, Date dateAdded, byte[] data) {
        this.id = id;
        this.filename = filename;
        this.type = type;
        this.dateAdded = dateAdded;
        this.data = data;
    }
}
