package com.kaributechs.insurehubdbengine.models.insurance_quotation;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RequestCompanyQuoteDTO {

    @NotNull(message = "insuranceId cannot be null")
    private Long insuranceId;

    @NotNull(message = "companyId cannot be null")
    private Long companyId;

    private String email;
    private String mobileNumber;
    private boolean sendEmail;

}
