package com.kaributechs.insurehubdbengine.models.insurance_quotation;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.InsurehubInsurance;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NewBusinessCompanyQuote {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String email;
    private String mobileNumber;

    @CreationTimestamp
    private Date date;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private InsurehubInsurance insurance;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("company_quotations")
    private Company company;

}
