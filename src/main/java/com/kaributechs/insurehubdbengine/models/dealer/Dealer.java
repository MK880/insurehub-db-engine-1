package com.kaributechs.insurehubdbengine.models.dealer;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
public class Dealer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String username;
    private String companyName;
    private String mobileNumber;
    private String email;

    @OneToMany(mappedBy = "dealer",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("dealer_service")
    private Set<DealerService> dealerServices;
    private boolean available;
    
}
