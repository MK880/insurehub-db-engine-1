package com.kaributechs.insurehubdbengine.models.dealer;

import lombok.Data;


@Data
public class DealerRequestQuotationDTO {

    private Long id;
    private boolean accident;
    private boolean towing;
    private String processId;

    private Long requestedServiceId;

    private DriverDetails driverDetails;

    private ServiceDetails serviceDetails;

    private VehicleDetails vehicleDetails;





}
