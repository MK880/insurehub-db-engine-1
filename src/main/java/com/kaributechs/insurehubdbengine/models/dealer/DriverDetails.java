package com.kaributechs.insurehubdbengine.models.dealer;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
public class DriverDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String driverNationalID;
    private String driverSalutation;
    private String driverLastName;
    private String driverFirstName;
    private String driverEmail;
    private String driverPhoneNumber;
}
