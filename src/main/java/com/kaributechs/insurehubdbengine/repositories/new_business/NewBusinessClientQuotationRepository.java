package com.kaributechs.insurehubdbengine.repositories.new_business;

import com.kaributechs.insurehubdbengine.models.insurance_quotation.NewBusinessClientQuotation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NewBusinessClientQuotationRepository extends JpaRepository<NewBusinessClientQuotation,Long> {
}
