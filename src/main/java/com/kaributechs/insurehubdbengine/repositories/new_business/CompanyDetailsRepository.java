package com.kaributechs.insurehubdbengine.repositories.new_business;

import com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance.CompanyDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyDetailsRepository extends JpaRepository<CompanyDetails,Long> {
}
