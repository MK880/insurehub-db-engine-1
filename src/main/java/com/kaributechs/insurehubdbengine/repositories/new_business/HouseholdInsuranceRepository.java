package com.kaributechs.insurehubdbengine.repositories.new_business;

import com.kaributechs.insurehubdbengine.models.new_business.household_insurance.HouseholdInsurance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface HouseholdInsuranceRepository extends JpaRepository<HouseholdInsurance,Long> {
    Optional<HouseholdInsurance> findHouseholdInsuranceByProcessId(String processId);
}
