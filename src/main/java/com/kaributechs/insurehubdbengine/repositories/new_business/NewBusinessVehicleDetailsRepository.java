package com.kaributechs.insurehubdbengine.repositories.new_business;

import com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance.CompanyInsuranceVehicleDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NewBusinessVehicleDetailsRepository extends JpaRepository<CompanyInsuranceVehicleDetails,Long> {
}
