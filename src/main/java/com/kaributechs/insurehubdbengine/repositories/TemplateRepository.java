package com.kaributechs.insurehubdbengine.repositories;


import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.Template;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TemplateRepository extends JpaRepository<Template,Long> {
    List<Template> findAllByCompany(Company company);
}
