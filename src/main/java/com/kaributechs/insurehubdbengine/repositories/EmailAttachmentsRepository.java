package com.kaributechs.insurehubdbengine.repositories;

import com.kaributechs.insurehubdbengine.models.EmailAttachment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailAttachmentsRepository extends JpaRepository<EmailAttachment,Long> {
}
