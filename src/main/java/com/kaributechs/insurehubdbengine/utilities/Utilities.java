package com.kaributechs.insurehubdbengine.utilities;

import com.kaributechs.insurehubdbengine.exceptions.GenericException;
import com.kaributechs.insurehubdbengine.models.dtos.FileUploadResponseDTO;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.representations.AccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

@Service
public class Utilities {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Qualifier("emailWebClient")
    private final WebClient emailWebClient;

    public Utilities(WebClient emailWebClient) {
        this.emailWebClient = emailWebClient;
    }

    public File convertMultipartFileToFile(MultipartFile file) throws IOException
    {
        logger.info("Converting {} to file",file.getOriginalFilename());
        String tempDirectory = System.getProperty("java.io.tmpdir");
        File convFile = new File(tempDirectory+"/"+file.getOriginalFilename());

        try (FileOutputStream fileOutputStream = new FileOutputStream(convFile)) {
            fileOutputStream.write(file.getBytes());
        } catch (Exception e) {
            logger.error(e.toString());
        }
        return convFile;
    }
    public String uploadDocument(File file,String type) {
        logger.info("uploadDocument");

        FileSystemResource value = new FileSystemResource(file);
        MultiValueMap<String, Object> formData = new LinkedMultiValueMap<>();
        formData.add("processId", "null");
        formData.add("type", type);
        formData.add("files", value);
        logger.info(value.getFilename());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new HttpEntity<>(formData, headers);

//        String serverUrl = "http://localhost:6004/api/v1/files/upload";
        String serverUrl = "http://files-engine:6004/api/v1/files/upload";


        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<FileUploadResponseDTO[]> responseDTOList = restTemplate.exchange(serverUrl, HttpMethod.POST, requestEntity, FileUploadResponseDTO[].class);
            List<FileUploadResponseDTO> object = Arrays.asList(responseDTOList.getBody());
            return object.get(0).getDownloadUrl();
        }catch (Exception e){
            throw new GenericException("Error From File Engine: "+e.getMessage());
        }

    }

    public String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof KeycloakPrincipal) {
            KeycloakPrincipal principal = (KeycloakPrincipal) authentication.getPrincipal();
            AccessToken accessToken = principal.getKeycloakSecurityContext().getToken();
            return accessToken.getPreferredUsername();
        } else return null;
    }

    public String getToken() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof KeycloakPrincipal) {
            KeycloakPrincipal principal = (KeycloakPrincipal) authentication.getPrincipal();
            return principal.getKeycloakSecurityContext().getTokenString();
        } else return null;
    }

    public static String[] getNullPropertyNames (Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<String>();
        for(java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) emptyNames.add(pd.getName());
        }

        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }

    public void sendEmailWithoutAttachment(MultiValueMap formData) {
        try {
            emailWebClient.post()
                    .uri("/email/send")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .body(BodyInserters.fromMultipartData(formData))
                    .retrieve().bodyToMono(Void.class).block();
        }catch (Exception e){
            throw new GenericException("Error while trying to send Email :"+e.getMessage());
        }

    }
    
    
}
