package com.kaributechs.insurehubdbengine.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebClientConfig {


    @Bean(name = "textGeneratorWebClient")
    public WebClient textGeneratorWebClient(){
//        String baseUrl = "http://localhost:5000/api/v1";
        String baseUrl = "http://text-generator:5001/api/v1";


        return WebClient.builder().baseUrl(baseUrl).build();
    }

    @Bean(name = "emailWebClient")
    public WebClient emailWebClient(){
//        String baseUrl = "http://localhost:6002/api/v1";
//        String baseUrl = "http://insurehubbackend.southafricanorth.cloudapp.azure.com:6002/api/v1";
        String baseUrl = "http://communicate-engine:6002/api/v1";


        return WebClient.builder().baseUrl(baseUrl).build();
    }


}
